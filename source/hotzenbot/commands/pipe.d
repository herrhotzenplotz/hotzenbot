/* Copyright 2020 Nico Sonack
 *
 * The contents of this file are subject to the terms of the Common
 * Development and Distribution License, Version 1.0 only (the
 * "License").  You may not use this file except in compliance with
 * the License.
 *
 * See the file LICENSE in this distribution for details.  A copy of
 * the CDDL is also available via the Internet at
 * http://www.opensource.org/licenses/cddl1.txt
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file LICENSE from this distribution.
 */

module hotzenbot.commands.pipe;

import std.algorithm;
import std.array;
import std.container.dlist;
import std.stdio;
import std.string;
import std.typecons;

import hotzenbot.commands.command;
import hotzenbot.commands.context;
import hotzenbot.sys.result;

class Pipe
{
    DList!CommandInvocation commands;

    EvalResult run(Context context)
    {
        EvalResult result = EvalResult.ok("");

        foreach (command; commands)
        {
            string arg = command.arg ~ result.result;
            context.variables["1"] = arg;

            result = command.run(context);
            if (result.is_error)
            {
                return result;
            }
        }

        return result;
    }

    void print()
    {
        foreach(cmd; commands)
        {
            writeln(cmd);
        }
    }

    static Nullable!Pipe parse(Context context, string input)
    {
        Pipe result = new Pipe();

        auto cmds = input.split("|").map!(x => x.strip);

        foreach (cmd; cmds)
        {
            if (!cmd.startsWith(context.call_prefix))
            {
                return Nullable!Pipe();
            }

            if (cmd.length == 1)
            {
                return Nullable!Pipe();
            }

            auto words = cmd[1..$].split(" ");
            CommandInvocation invoc;
            invoc.command_name = words[0].strip;
            invoc.arg = words[1..$].join(" ").strip;
            result.commands.insertBack(invoc);
        }

        return Nullable!Pipe(result);
    }
}
