/* Copyright 2020 Nico Sonack
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License, Version 1.0 only
 * (the "License").  You may not use this file except in compliance
 * with the License.
 *
 * See the file LICENSE in this distribution for details.
 * A copy of the CDDL is also available via the Internet at
 * http://www.opensource.org/licenses/cddl1.txt
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file LICENSE from this distribution.
 */

module hotzenbot.commands.command;

import hotzenbot.commands.context;
import hotzenbot.commands.expression;
import hotzenbot.commands.expressionparser;
import hotzenbot.sys.database;
import hotzenbot.sys.logging;
import hotzenbot.sys.result;

import std.conv : to;

import d2sqlite3;

struct Command
{
    string name;

    Expression expression;
}

struct CommandInvocation
{
    string arg;
    string command_name;

    // TODO(#27): Reduce number of db queries in CommandInvocation.run
    EvalResult run(Context context)
    {
        auto command_code = context.database.command_by_name(command_name);
        if (command_code.is_error)
        {
            log_error(LoggingScope.System, "Cannot find command " ~ command_name ~ ": " ~ command_code.err_msg);
            return EvalResult.fail("Command `" ~ command_name ~ "` not found");
        }

        string final_args = arg;
        if ("1" in context.variables)
            final_args = context.variables["1"];

        int command_id = context.command_id(command_name).get;

        context.log_command_call(command_id, final_args);
        auto bump_result = context.database.bump_command_times(command_id);
        if (bump_result.is_error)
        {
            log_error(LoggingScope.System, bump_result.err_msg);
            return EvalResult.fail("Unable to run command " ~ command_name);
        }

        context.variables["times"] = context.times_command(command_name).get.to!string;

        ExpressionParser parser = ExpressionParser(command_code.result);
        auto parse_result = parser.parse();

        if (parse_result.is_error)
        {
            return EvalResult.fail(parse_result.err_msg);
        }

        return parse_result.result.evaluate(context);
    }
}

Result!int add_command(Database* db, string command_name, string command_code)
{
    try
    {
        db.hotzenbot_exec("INSERT INTO Command (code) VALUES (:command_code) ;", command_code);
        int id = cast(int)(db.lastInsertRowid);
        db.hotzenbot_exec("INSERT INTO CommandName (name, commandId) VALUES (:command_name, :command_id) ;", command_name, id);
        return Result!int.ok(id);
    }
    catch (Exception e)
    {
        return Result!int.fail("Unable to add command: " ~ e.to!string);
    }
}

Result!int add_alias(Database* db, string command_name, string alias_name)
{
    auto id = db.command_id(command_name);
    if (id.is_error)
    {
        return id;
    }

    try
    {
        db.hotzenbot_exec("INSERT INTO CommandName (name, commandId) VALUES (:command_name, :command_id) ;", alias_name, id.result);
        return id;

    }
    catch (Exception e)
    {
        return Result!int.fail("Unable to add command: " ~ e.to!string);
    }
}

Result!int command_id(Database* db, string command_name)
{
    try
    {
        auto query_result = db.hotzenbot_exec("SELECT commandId FROM CommandName WHERE name = :command_name;", command_name);
        if (query_result.empty)
        {
            return Result!int.fail("No command with name " ~ command_name ~ " found.");
        }

        return Result!int.ok(query_result.front.peek!int(0));
    }
    catch (Exception e)
    {
        return Result!int.fail("Unable to fetch command id: " ~ e.to!string);
    }
}

Result!string delete_command_by_id(Database* db, int command_id)
{
    try
    {
        db.hotzenbot_exec("DELETE FROM Command WHERE id = :command_id;", command_id);
        return Result!string.ok(null);
    }
    catch (Exception e)
    {
        return Result!string.fail("Unable to delete command with id " ~ command_id.to!string ~ ": " ~ e.to!string);
    }
}

Result!string delete_command_by_name(Database* db, string command_name)
{
    try
    {
        auto maybe_id = db.command_id(command_name);
        if (maybe_id.is_error)
        {
            return Result!string.fail(maybe_id.err_msg);
        }

        return db.delete_command_by_id(maybe_id.result);

    }
    catch (Exception e)
    {
        return Result!string.fail("Unable to delete command: " ~ e.to!string);
    }
}

Result!string delete_command_name(Database* db, string command_name)
{
    try
    {
        db.hotzenbot_exec("DELETE FROM CommandName WHERE name = :command_name ;", command_name);
        return Result!string.ok(null);
    }
    catch (Exception e)
    {
        return Result!string.fail("Unable to delete command name " ~ command_name ~ ": " ~ e.to!string);
    }
}

Result!int command_times(Database* db, string command_name)
{
    try
    {
        auto times = db.hotzenbot_exec("SELECT c.times FROM Command c INNER JOIN CommandName cn ON c.id = cn.commandId  WHERE cn.name = :command_name ;", command_name);
        if (times.empty)
        {
            return Result!int.fail("No such command");
        }

        return Result!int.ok(times.front.peek!int(0));
    }
    catch (Exception e)
    {
        return Result!int.fail("Unable to fetch command-times: " ~ e.to!string);
    }
}

Result!string bump_command_times(Database* db, int command_id)
{
    try
    {
        db.hotzenbot_exec("UPDATE Command SET times = times + 1 WHERE id = :command_id ;", command_id);
        return Result!string.ok(null);
    }
    catch (Exception e)
    {
        return Result!string.fail("Unable to bump command invocation count: " ~ e.to!string);
    }
}

Result!string command_by_name(Database* db, string command_name)
{
    try
    {
        auto query_result = db.hotzenbot_exec("SELECT c.code FROM Command c INNER JOIN CommandName cn ON c.id = cn.commandId  WHERE cn.name = :command_name ;", command_name);

        if (query_result.empty)
        {
            return Result!string.fail("No such command");
        }
        else
        {
            return Result!string.ok(query_result.front.peek!string(0));
        }
    }
    catch (Exception e)
    {
        return Result!string.fail("Unable to get command by name: " ~ e.to!string);
    }
}
