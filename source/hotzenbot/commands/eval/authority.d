/* Copyright 2020 Nico Sonack
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License, Version 1.0 only
 * (the "License").  You may not use this file except in compliance
 * with the License.
 *
 * See the file LICENSE in this distribution for details.
 * A copy of the CDDL is also available via the Internet at
 * http://www.opensource.org/licenses/cddl1.txt
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file LICENSE from this distribution.
 */

module hotzenbot.commands.eval.authority;

import std.conv : to;
import std.json;
import std.net.curl;
import std.range;

import hotzenbot.bot.globalsignal;
import hotzenbot.bot.roles;
import hotzenbot.commands.command;
import hotzenbot.commands.context;
import hotzenbot.commands.expression;
import hotzenbot.commands.irccontext;
import hotzenbot.commands.replcontext;
import hotzenbot.commands.twitchcontext;
import hotzenbot.sys.database;
import hotzenbot.sys.logging;
import hotzenbot.sys.result;
import hotzenbot.sys.twitchapi;

import d2sqlite3;

Result!int user_id_of_twitch_user(TwitchAuth credentials, string user_name)
{
    auto user_id_response = perform_twitch_api_request(credentials, "https://api.twitch.tv/helix/users?login=" ~ user_name);
    if (user_id_response.is_error)
    {
        return Result!int.fail(user_id_response.err_msg);
    }

    auto parsed_response = parseJSON(user_id_response.result);
    if ("data" !in parsed_response.object)
    {
        log_error(LoggingScope.Twitch, "No data field in Twitch API reponse");
        return Result!int.fail("Invalid response from Twitch API");
    }

    if (parsed_response.object["data"].array.empty)
    {
        return Result!int.fail("No such user.");
    }

    return Result!int.ok(parsed_response.object["data"].array[0].object["id"].str.to!int);
}

EvalResult ass_role(Context ctx, string user_name, string role_name)
{
    auto tctx = cast(TwitchContext)(ctx);
    if (tctx !is null)
    {
        return assign_role_to_twitch_user(ctx, user_name, role_name, tctx.credentials);
    }

    auto rctx = cast(ReplContext)(ctx);
    if (rctx !is null)
    {
        return assign_role_to_twitch_user(ctx, user_name, role_name, TwitchAuth(rctx.access_token, rctx.client_id));
    }

    auto ictx = cast(IRCContext)(ctx);
    if (ictx !is null)
    {
        if (user_name in ictx.channel_state.addresses)
        {
            return assign_role_to_irc_user(ctx, user_name, role_name, ictx.network, ictx.channel_state.addresses[user_name]);
        }
        else
        {
            return EvalResult.fail("User " ~ user_name ~ " is not joined to this channel.");
        }
    }

    return EvalResult.fail("Unknown context type");
}

EvalResult assign_role_to_irc_user(Context ctx, string user_nick, string role_name, string network, string user_address)
{
    try
    {
        auto _role_id = ctx.database.get_twitch_role_id_by_name(role_name);
        if (_role_id.is_error)
        {
            return EvalResult.fail(_role_id.err_msg);
        }

        int role_id = _role_id.result;

        ctx.database.hotzenbot_exec("INSERT INTO IrcUserRoles (userNetwork, userNickname, userAddress, roleId) " ~
                                    "VALUES (:user_network, :user_nick, :user_addr, :role_id);",
                                    network, user_nick, user_address, role_id);
        log_info(LoggingScope.IRC, "Assigned role `" ~ role_name ~ "` to user " ~ user_nick);
        return EvalResult.ok("Assigned role `" ~ role_name ~ "` to user " ~ user_nick ~ ".");
    }
    catch (Exception e)
    {
        log_error(LoggingScope.IRC, "Failed to assign user role: " ~ e.to!string);
        return EvalResult.fail("Unable to assign user role. Check the logs.");
    }
}

EvalResult assign_role_to_twitch_user(Context ctx, string user_name, string role_name, TwitchAuth credentials)
{
    try
    {
        auto _user_id = user_id_of_twitch_user(credentials, user_name);
        if (_user_id.is_error)
        {
            return EvalResult.fail(_user_id.err_msg);
        }
        int user_id = _user_id.result;

        auto _role_id = ctx.database.get_twitch_role_id_by_name(role_name);
        if (_role_id.is_error)
        {
            return EvalResult.fail(_role_id.err_msg);
        }

        int role_id = _role_id.result;

        ctx.database.hotzenbot_exec("INSERT INTO TwitchUserRoles (userId, roleId) VALUES (:user_id, :role_id);", user_id, role_id);
        log_info(LoggingScope.Twitch, "Assigned role `" ~ role_name ~ "` to " ~ user_name ~ ".");
        return EvalResult.ok("Assigned role `" ~ role_name ~ "` to " ~ user_name ~ ".");
    }
    catch (Exception e)
    {
        log_error(LoggingScope.Twitch, "Unable to assign role: " ~ e.to!string);
        return EvalResult.fail("Unable to assign role. Please check the logs");
    }
}

EvalResult revoke_role(Context ctx, string user_name, string role_name)
{
    TwitchContext tctx = cast(TwitchContext)ctx;
    if (tctx !is null)
    {
        return revoke_role_of_twitch_user(ctx, user_name, role_name, tctx.credentials);
    }

    auto ictx = cast(IRCContext)(ctx);
    if (ictx !is null)
    {
        if (user_name in ictx.channel_state.addresses)
        {
            return revoke_role_of_irc_user(ctx, user_name, role_name, ictx.network, ictx.channel_state.addresses[user_name]);
        }
        else
        {
            return EvalResult.fail("User " ~ user_name ~ " is not joined to this channel.");
        }
    }

    return EvalResult.fail("Unable to revoke roles in this context.");
}

EvalResult revoke_role_of_irc_user(Context ctx, string user_nick, string role_name, string network, string user_address)
{
    try
    {
        auto _role_id = ctx.database.get_twitch_role_id_by_name(role_name);
        if (_role_id.is_error)
        {
            return EvalResult.fail(_role_id.err_msg);
        }

        int role_id = _role_id.result;

        ctx.database.hotzenbot_exec("DELETE FROM IrcUserRoles WHERE userNetwork = :net " ~
                                    "AND userNickName = :nick AND userAddress = :addr AND roleId = :id;",
                                    network, user_nick, user_address, role_id);
        return EvalResult.ok("Revoked role " ~ role_name ~ " from user " ~ user_nick);
    }
    catch (Exception e)
    {
        log_error(LoggingScope.IRC, "Unable to revoke role: " ~ e.to!string);
        return EvalResult.fail("Unable to revoke role");
    }
}

EvalResult revoke_role_of_twitch_user(Context ctx, string user_name, string role_name, TwitchAuth credentials)
{
    try
    {
        auto _user_id = user_id_of_twitch_user(credentials, user_name);
        if (_user_id.is_error)
        {
            return EvalResult.fail(_user_id.err_msg);
        }
        int user_id = _user_id.result;

        auto _role_id = ctx.database.get_twitch_role_id_by_name(role_name);
        if (_role_id.is_error)
        {
            return EvalResult.fail(_role_id.err_msg);
        }

        int role_id = _role_id.result;

        ctx.database.hotzenbot_exec("DELETE FROM TwitchUserRoles WHERE userId = :user_id AND roleId = :role_id;",
                                    user_id, role_id);
        log_info(LoggingScope.Twitch, "Revoked role `" ~ role_name ~ "` from " ~ user_name ~ ".");
        return EvalResult.ok("Revoked role " ~ role_name ~ " from user " ~ user_name);
    }
    catch (Exception e)
    {
        return EvalResult.fail("Unable to revoke role of user " ~ user_name);
    }
}

EvalResult join(string args)
{
    auto pair = args.split('/');
    if (pair.length != 2)
    {
        return EvalResult.fail("Unable to parse channel. Expected format: transport/#channel");
    }

    join_channel(pair[0], pair[1]);

    return EvalResult.ok("Join signal sent. This may fail in the backend.");
}

EvalResult part(string args)
{
    auto pair = args.split('/');
    if (pair.length != 2)
    {
        return EvalResult.fail("Unable to parse channel. Expected format: transport/#channel");
    }

    part_channel(pair[0], pair[1]);

    return EvalResult.ok("Part signal sent. This may fail in the backend.");
}

EvalResult addcmd(Context ctx, Expression[] exprs)
{
    if (!ctx.is_sender_maintainer)
    {
        return EvalResult.fail("Only for the bot maintainer");
    }

    if (exprs.length < 2)
    {
        return EvalResult.fail("Expected at least two arguments for add_command");
    }

    auto maybe_command_name = exprs[0].evaluate(ctx);
    if (maybe_command_name.is_error)
    {
        return maybe_command_name;
    }

    auto maybe_command_code = evaluate_concat_all(ctx, exprs[1..$]);
    if (maybe_command_code.is_error)
    {
        return maybe_command_code;
    }

    auto db_res = add_command(ctx.database, maybe_command_name.result, maybe_command_code.result);
    if (db_res.is_error)
    {
        log_error(LoggingScope.System, db_res.err_msg);
        return EvalResult.fail("Unable to add command " ~ maybe_command_name.result);
    }
    else
    {
        return EvalResult.ok("Added command");
    }
}

EvalResult delcmd(Context ctx, string cmd_name)
{
    if (!ctx.is_sender_maintainer)
    {
        return EvalResult.fail("Only for bot maintainers");
    }

    auto db_result = delete_command_by_name(ctx.database, cmd_name);
    if (db_result.is_error)
    {
        log_error(LoggingScope.System, db_result.err_msg);
        return EvalResult.fail("Unable to delete command");
    }

    return EvalResult.ok("Deleted command");
}

EvalResult _add_hook(Database *db,
                     string network, string channel,
                     string trigger_word, string code)
{
    try
    {
        db.hotzenbot_exec("INSERT INTO Hooks (network, channel, triggerWord, code) VALUES (:net, :chan, :trig, :code);",
                          network, channel, trigger_word, code);
        reload_hooks();
        log_warn(LoggingScope.System, "Added hook `" ~ trigger_word ~ "`");

        return EvalResult.ok("Hook added");
    }
    catch (Exception e)
    {
        log_error(LoggingScope.System, "Unable to add hook: " ~ e.to!string);
        return EvalResult.fail("Unable to add hook");
    }
}

EvalResult add_hook(Context ctx, string trigger_word, string code)
{
    auto tctx = cast(TwitchContext)(ctx);
    if (tctx !is null)
    {
        return _add_hook(ctx.database, "twitch", tctx.channel, trigger_word, code);
    }

    auto ictx = cast(IRCContext)(ctx);
    if (ictx !is null)
    {
        return _add_hook(ctx.database, ictx.network, ictx.channel, trigger_word, code);
    }

    return EvalResult.fail("Adding hooks is not supported in this context");
}

EvalResult _delete_hook(Database *db,
                        string network, string channel,
                        string trigger_word)
{
    try
    {
        db.hotzenbot_exec("DELETE FROM Hooks WHERE network = :net AND channel = :channel AND triggerWord = :wrd;",
                          network, channel, trigger_word);
        reload_hooks();
        log_warn(LoggingScope.System, "Deleted hook `" ~ trigger_word ~ "`");
        return EvalResult.ok("Hook has been deleted");
    }
    catch (Exception e)
    {
        log_error(LoggingScope.System, "Unable to delete hook: " ~ e.to!string);
        return EvalResult.fail("Unable to delete hook");
    }
}

EvalResult delete_hook(Context ctx, string trigger_word)
{
    auto tctx = cast(TwitchContext)(ctx);
    if (tctx !is null)
    {
        return _delete_hook(ctx.database, "twitch", tctx.channel, trigger_word);
    }

    auto ictx = cast(IRCContext)(ctx);
    if (ictx !is null)
    {
        return _delete_hook(ctx.database, ictx.network, ictx.channel, trigger_word);
    }

    return EvalResult.fail("Deleting hooks is not supported in this context");
}
