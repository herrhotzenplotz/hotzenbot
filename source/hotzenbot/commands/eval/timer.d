/* Copyright 2020 Nico Sonack
 *
 * The contents of this file are subject to the terms of the Common
 * Development and Distribution License, Version 1.0 only (the
 * "License").  You may not use this file except in compliance with
 * the License.
 *
 * See the file LICENSE in this distribution for details.  A copy of
 * the CDDL is also available via the Internet at
 * http://www.opensource.org/licenses/cddl1.txt
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file LICENSE from this distribution.
 */

module hotzenbot.commands.eval.timer;

import hotzenbot.bot.timerthread;
import hotzenbot.commands.context;
import hotzenbot.commands.irccontext;
import hotzenbot.commands.twitchcontext;
import hotzenbot.sys.database;
import hotzenbot.sys.logging;
import hotzenbot.sys.result;

import std.algorithm;
import std.array;
import std.conv;
import std.datetime;
import std.range;
import std.string;

import d2sqlite3;

Result!SysTime parse_time(string when)
{
    Duration d;

    when = when.replace("and", "");

    try
    {
        foreach(pair; when.split.chunks(2))
        {
            if (pair.length != 2)
            {
                return Result!SysTime.fail("Need a unit of time");
            }

            pair[0] = pair[0].strip;

            switch (pair[1].strip)
            {
            case "min":
            case "m":
            case "minutes":
            case "mins": {
                d += dur!"minutes"(parse!long(pair[0]));
            } break;
            case "secs":
            case "s":
            case "seconds":
            case "sec": {
                d += dur!"seconds"(parse!long(pair[0]));
            } break;
            case "hours":
            case "h":
            case "hrs":
            case "hour": {
                d += dur!"hours"(parse!long(pair[0]));
            } break;
            case "days":
            case "d":
            case "a": {
                d += dur!"days"(parse!long(pair[0]));
            } break;
            default: {
                return Result!SysTime.fail("Unknown time unit " ~ pair[1]);
            }
            }
        }
    }
    catch (Exception e)
    {
        log_error(LoggingScope.Timers, "Unable to parse duration: " ~ e.to!string);
        return Result!SysTime.fail("Unable to parse duration");
    }

    return Result!SysTime.ok(Clock.currTime() + d);
}

EvalResult irc_set_timer(IRCContext ctx, string message, SysTime when)
{
    ctx.database.hotzenbot_exec("INSERT INTO Timers(network, channel, userName, message, dueTime) VALUES " ~
                                "(:network, :channel, :user, :msg, :due);",
                                ctx.network, ctx.channel, ctx.sender, message, when.toISOString);
    add_oneshottimer(OneshotTimer(ctx.network, ctx.channel, ctx.sender, message, when, ctx.database.lastInsertRowid));
    return EvalResult.ok("Added timer");
}

EvalResult twitch_set_timer(TwitchContext ctx, string message, SysTime when)
{
    ctx.database.hotzenbot_exec("INSERT INTO Timers(network, channel, userName, message, dueTime) VALUES " ~
                                "(:network, :channel, :user, :msg, :due);",
                                "twitch", ctx.channel, ctx.sender, message, when.toISOString);
    add_oneshottimer(OneshotTimer("twitch", ctx.channel, ctx.sender, message, when, ctx.database.lastInsertRowid));
    return EvalResult.ok("Added timer");
}

EvalResult set_timer(Context ctx, string message, string duration)
{
    try
    {
        auto time = parse_time(duration);
        if (time.is_error)
        {
            return EvalResult.fail(time.err_msg);
        }

        TwitchContext tctx = cast(TwitchContext)ctx;
        if (tctx)
        {
            return twitch_set_timer(tctx, message, time.result);
        }

        IRCContext ictx = cast(IRCContext)ctx;
        if (ictx)
        {
            return irc_set_timer(ictx, message, time.result);
        }
    }
    catch (Exception e)
    {
        log_error(LoggingScope.Timers, "Unable to set timer: " ~ e.to!string);
    }

    return EvalResult.fail("Unable to set a timer from a non-IRC or non-Twitch context.");
}
