/*
 * Copyright 2020 Nico Sonack
 */

module hotzenbot.commands.eval.calc;

import hotzenbot.commands.context;
import hotzenbot.sys.logging;
import hotzenbot.sys.result;

import std.algorithm;
import std.conv : to;
import std.functional : toDelegate;
import std.math;
import std.string;

/* Stolen and heavily modified from:
 * https://github.com/PhilippeSigaud/Pegged/blob/master/examples/simple_arithmetic/src/pegged/examples/simple_arithmetic.d
 * LICENSE: Boost License
 */
import pegged.grammar;

mixin(grammar(`
Arithmetic:
    Command  < Def / Term
    Def      < identifier "=" Term
    Term     < Factor (Add / Sub)*
    Add      < "+" Factor
    Sub      < "-" Factor
    Factor   < Primary (Mul / Div)*
    Mul      < "*" Primary
    Div      < "/" Primary
    Primary  < Parens / Neg / Number / Function / Variable
    Parens   < "(" Term ")"
    Neg      < "-" Primary
    Number   <~ [0-9]+ ( ('.' [0-9]+) / ( "e" "-"? [0-9]+ ) )?
    Arglist  < Term ( "," Term)*
    Function <- identifier "(" Arglist? ")"
    Variable <- identifier
`));

alias CalcResult = Result!double;

mixin template OneArg(string name) {
    mixin(`CalcResult c_%s(double[] args)
           {
               return
                 (args.length != 1)
                 ? CalcResult.fail("\"%s\" expects one argument")
                 : CalcResult.ok(%s(args[0]));
           }
           `.format(name, name, name));
}

mixin template TwoArgs(string name) {
    mixin(`CalcResult c_%s(double[] args)
           {
               return
                 (args.length != 2)
                 ? CalcResult.fail("\"%s\" expects two arguments")
                 : CalcResult.ok(%s(args[0], args[1]));
           }
           `.format(name, name, name));
}

CalcResult c_random(double[] args)
{
    import std.random;
    if (!args.empty)
    {
        return CalcResult.fail("random expects no arguments");
    }

    return CalcResult.ok(uniform(0.0, 1.0, rndGen));
}

CalcResult eval_expr(Context ctx, string expr)
{
    const double[string] variables = [
        "pi": PI,
        "e" : E,
        "tau": 2.0*PI,
        "phi": (1.0 + sqrt(5.0)) * 0.5];

    /*
     * This monstrosity bakes functions from std.math using
     * CTFE into a hash-table that is generated at compile-time.
     * Those functions are then callable from the %calc() function in the bot
     */
    // BEGIN CODE-GEN
    const string[] f_single_arg_list = ["sin", "cos", "tan", "asin", "acos", "atan",
                                        "sinh", "cosh", "tanh", "asinh", "acosh", "atanh",
                                        "sqrt", "fabs", "exp", "log", "log10", "log2",
                                        "floor", "ceil", "trunc" ];
    const string[] f_two_args_list = ["pow", "fmod"];
    const string functions_to_bake = (f_two_args_list ~ f_single_arg_list)
        .map!(name => `"%s": &c_%s,`.format(name, name)).join;
    static foreach (name; f_single_arg_list) mixin OneArg!name;
    static foreach (name; f_two_args_list) mixin TwoArgs!name;

    mixin("const CalcResult delegate(double[])[string] functions =
        [ \"random\": toDelegate(&c_random), %s
        ];".format(functions_to_bake));
    // END CODE-GEN

    if (expr == "help")
    {
        return CalcResult.fail("Any mathematical expression with +,-,*,/. "~
                               "Constants: " ~ variables.keys.join(",") ~ "." ~
                               "Functions with one arg: " ~ f_single_arg_list.join(",") ~
                               ". Functions with two args: " ~ f_two_args_list.join(",") ~ ".");
    }

    auto tree = Arithmetic(expr);

    if (tree.end < expr.length)
    {
        return CalcResult.fail("parse error: unmatched input. Invoke the calc command with `help` for more details.");
    }

    CalcResult value(ParseTree p)
    {
        switch (p.name)
        {
        case "Arithmetic":
            return value(p.children[0]);
        case "Arithmetic.Command":
            return value(p.children[0]);
        case "Arithmetic.Def":
            auto var_name = p.matches[0];
            auto var_value = value(p.children[0]);
            if (var_value.is_error)
            {
                return var_value;
            }

            auto res = ctx.set_calc_var(var_name, var_value.result);
            if (res.is_error)
            {
                return CalcResult.fail(res.err_msg);
            }
            else
            {
                return var_value;
            }

        case "Arithmetic.Term":
            double v = 0.0;
            foreach(child; p.children) {
                auto _v = value(child);
                if (_v.is_error)
                {
                    return _v;
                }
                v += _v.result;
            }
            return CalcResult.ok(v);
        case "Arithmetic.Add":
            return value(p.children[0]);
        case "Arithmetic.Sub":
            auto temp = value(p.children[0]);
            if (temp.is_error)
            {
                return temp;
            }
            return CalcResult.ok(-temp.result);
        case "Arithmetic.Factor":
            double v = 1.0;
            foreach(child; p.children) {
                auto _v = value(child);
                if (_v.is_error)
                {
                    return _v;
                }
                v *= _v.result;
            }
            return CalcResult.ok(v);
        case "Arithmetic.Mul":
            return value(p.children[0]);
        case "Arithmetic.Div":
            auto temp = value(p.children[0]);
            if (temp.is_error)
            {
                return temp;
            }

            // TODO(#34): Maybe use correct double-precision float epsilons
            if (temp.result.approxEqual(0.0, 1e-20, 1e-30))
            {
                return CalcResult.fail("Division by zero is undefined");
            }

            return CalcResult.ok(1.0 / temp.result);
        case "Arithmetic.Primary":
            return value(p.children[0]);
        case "Arithmetic.Parens":
            return value(p.children[0]);
        case "Arithmetic.Neg":
            auto temp = value(p.children[0]);
            if (temp.is_error)
            {
                return temp;
            }
            return CalcResult.ok(-temp.result);
        case "Arithmetic.Number":
            return CalcResult.ok(to!double(p.matches[0]));
        case "Arithmetic.Function":
            if (p.matches[0] !in functions)
            {
                return CalcResult.fail("undefined is not a function FeelsDankMan");
            }

            double[] args;
            foreach (child; p.children[0].children)
            {
                auto _arg = value(child);
                if (_arg.is_error)
                {
                    return _arg;
                }
                args ~= _arg.result;
            }

            return functions[p.matches[0]](args);
        case "Arithmetic.Variable":
            if (p.matches[0] in variables)
            {
                return CalcResult.ok(variables[p.matches[0]]);
            }
            else if (p.matches[0] in functions)
            {
                return functions[p.matches[0]]([]);
            }
            else
            {
                return ctx.get_calc_var(p.matches[0]);
            }
        default:
            return CalcResult.fail("internal parser failure");
        }
    }

    return value(tree);
}

EvalResult calc(Context ctx, string expr)
{
    auto eval_result = eval_expr(ctx, expr);

    if (eval_result.is_error)
    {
        return EvalResult.fail(eval_result.err_msg);
    }

    return EvalResult.ok(eval_result.result.to!string);
}
