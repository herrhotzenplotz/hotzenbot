/* Copyright 2020 Nico Sonack
 *
 * The contents of this file are subject to the terms of the Common
 * Development and Distribution License, Version 1.0 only (the
 * "License").  You may not use this file except in compliance with
 * the License.
 *
 * See the file LICENSE in this distribution for details.  A copy of
 * the CDDL is also available via the Internet at
 * http://www.opensource.org/licenses/cddl1.txt
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file LICENSE from this distribution.
 */

module hotzenbot.irc.bufferedsocket;

import std.algorithm;
import std.stdio;
import std.socket;
import std.string;
import std.container.dlist;

import hotzenbot.sys.logging;

class BufferedTcpSocket : TcpSocket
{
    string read_line()
    {
        while (incoming_buffer.empty)
        {
            receive_buf.fill('\0');
            long received_bytes = receive(receive_buf);

            if (should_exit)
            {
                return null;
            }

            if (received_bytes == -1) continue;

            if (received_bytes == 0 || received_bytes == Socket.ERROR)
            {
                log_fatal(LoggingScope.System, "Connection has been reset or some other error occured");
                return "";
            }

            string message = rest ~ cast(string)(receive_buf[0..received_bytes].dup);

            while (message.canFind("\r\n"))
            {
                auto split = message.findSplit("\r\n");
                if (split[0].strip.empty)
                {
                    log_warn(LoggingScope.System, "Got empty line");
                }
                else
                {
                    incoming_buffer.insertBack(split[0]);
                }
                message = split[2];
            }

            rest = message.dup;
        }

        auto next = incoming_buffer.front();
        incoming_buffer.removeFront();
        return next;
    }

    this(Address addr)
    {
        super(addr);
        this.receive_buf = new char[512];
    }

    DList!string incoming_buffer;
    shared bool should_exit = false;
    string rest = "";
    char[512] receive_buf;
}
