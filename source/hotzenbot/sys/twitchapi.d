/* Copyright 2020 Nico Sonack
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License, Version 1.0 only
 * (the "License").  You may not use this file except in compliance
 * with the License.
 *
 * See the file LICENSE in this distribution for details.
 * A copy of the CDDL is also available via the Internet at
 * http://www.opensource.org/licenses/cddl1.txt
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file LICENSE from this distribution.
 */

module hotzenbot.sys.twitchapi;

import hotzenbot.sys.logging;
import hotzenbot.sys.result;

import std.conv;
import std.net.curl;

struct TwitchAuth
{
    string access_token;
    string client_id;
}

Result!string perform_twitch_api_request(TwitchAuth credentials, string url)
{
    try
    {
        auto client = HTTP();
        client.addRequestHeader("Authorization", "Bearer " ~ credentials.access_token);
        client.addRequestHeader("Client-ID", credentials.client_id);
        return Result!string.ok(cast(string)(get(url, client)));
    }
    catch (Exception e)
    {
        log_error(LoggingScope.Twitch, "Twitch API request failed: " ~ e.to!string);
        return Result!string.fail("Twitch API request failed");
    }
}
