/* Copyright 2020 Nico Sonack
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License, Version 1.0 only
 * (the "License").  You may not use this file except in compliance
 * with the License.
 *
 * See the file LICENSE in this distribution for details.
 * A copy of the CDDL is also available via the Internet at
 * http://www.opensource.org/licenses/cddl1.txt
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file LICENSE from this distribution.
 */

module hotzenbot.bot.markovthread;

import std.algorithm;
import std.array;
import std.concurrency;
import std.conv : to;
import std.random;
import std.range;
import std.string;
import std.typecons;

import core.thread;

import d2sqlite3;

import hotzenbot.sys.database;
import hotzenbot.sys.logging;

__gshared Tid markov_tid;
__gshared MarkovModel markov_model;

struct MarkovPair
{
    string event1;
    string event2;
    int n;
}

string gen_markov_sentence(string seed)
{
    return markov_model.generate_sentence(seed);
}

MarkovPair[] get_n_most_likely_events(int n, string word)
{
    return markov_model.get_n_most_likely_events(n, word);
}

struct MarkovModel
{
    MarkovNextChain[string] events;
    alias events this;

    this(Database* database)
    {
        log_info(LoggingScope.Markov, "Initializing markov cache");
        auto rows = database.hotzenbot_exec("SELECT * FROM Markov;");

        foreach (row; rows)
        {
            add_pair(MarkovPair(row["event1"].as!string, row["event2"].as!string, row["n"].as!int));
        }

        log_info(LoggingScope.Markov, "Successfully initialized markov cache");
    }

    void add_pair(string evt1, string evt2)
    {
        if (evt1 in events)
        {
            events[evt1][evt2] += 1;
        }
        else
        {
            events[evt1] = MarkovNextChain();
            events[evt1][evt2] = 1;
        }
    }

    MarkovPair[] get_n_most_likely_events(int n, string word)
    {
        auto event = "Word \"" ~ word.strip ~ "\"";
        if (event !in events)
        {
            return [];
        }

        return events[event].frequencies
            .byPair
            .array
            .sort!((a, b) => a.value > b.value)
            .take(n)
            .map!(x => MarkovPair(word, x.key, x.value))
            .array;
    }

    void add_pair(MarkovPair pair)
    {
        if (pair.event1 !in events)
        {
            events[pair.event1] = MarkovNextChain();
        }

        if (pair.event2 !in events[pair.event1])
        {
            events[pair.event1][pair.event2] = pair.n;
        }
        else
        {
            assert(0, "Trying to add a Markov Pair that already exists");
        }
    }

    string next_markov_event(string event)
    {
        alias ordering = (x, y) => x[1] > y[1];

        if (event !in events) return "End";
        auto rows = events[event].next_events
            .map!(x => tuple(x, events[event].frequencies[x]))
            .array;

        sort!ordering(rows);

        int n = rows.map!(x => x[1]).sum;
        int i = uniform(0, n);

        int curr = -1;
        foreach (row; rows)
        {
            curr += row[1];
            if (curr >= i)
            {
                return row[0];
            }
        }

        return "End";
    }

    string event_to_text(string event)
    {
        if (event == "Begin" || event == "End")
        {
            return "";
        }
        else
        {
            string tmp = event.drop(5);
            return tmp[1..$-1];
        }
    }

    string generate_sentence(string seed)
    {
        string result_sentence;
        string current_event = "Begin";
        if (!seed.strip.empty)
        {
            // REEeee REEeee REEeee Merge requests trying to "fix this
            // code" are immediately rejected without any further
            // discussion.
            auto words = seed.strip.split();
            current_event = "Word \"" ~ words[words.length - 1] ~ "\"";
            result_sentence = seed;
        }

        for (;;)
        {
            auto next_event = next_markov_event(current_event);
            if (next_event == "End")
            {
                return result_sentence;
            }

            if (!result_sentence)
            {
                result_sentence = event_to_text(next_event);
            }
            else
            {
                result_sentence ~= " " ~ event_to_text(next_event);
            }

            current_event = next_event;
        }

        assert(0);
    }
}

struct MarkovNextChain
{
    int[string] frequencies;

    auto next_events()
    {
        return frequencies.keys;
    }

    alias frequencies this;
}

class MarkovThread : Thread
{
    this(Database* database)
    {
        super(&this.markov_thread_worker);
        this.database = database;
        markov_model = MarkovModel(database);
    }

    struct RetrainMessage {}

    MarkovPair get_markov_pair(Database* database, string event1, string event2)
    {
        auto rows = database.hotzenbot_exec("SELECT n FROM Markov WHERE event1 = :event1 AND event2 = :event2;",
                                            event1, event2);
        if (rows.empty) {
            return MarkovPair(event1, event2, 0);
        } else {
            return MarkovPair(event1, event2, rows.front.peek!int(0));
        }
    }

    void insert_markov_pair(Database* db, MarkovPair pair)
    {
        database.hotzenbot_exec("INSERT INTO Markov (event1, event2, n) VALUES (:event1, :event2, :n);",
                                pair.event1, pair.event2, pair.n + 1);
        markov_model.add_pair(pair.event1, pair.event2);
    }

    void add_sentence_to_markov(Database* database, string sentence)
    {
        auto words = sentence.split;
        foreach(pair; words.slide(2)) {
            if (pair.length != 2) continue;

            MarkovPair markovPair = get_markov_pair(database, "Word \"" ~ pair[0] ~ "\"", "Word \"" ~ pair[1] ~ "\"");
            insert_markov_pair(database, markovPair);
        }

        // Insert start and end of the sentence
        MarkovPair start_pair = get_markov_pair(database, "Begin", "Word \"" ~ words[0] ~ "\"");
        insert_markov_pair(database, start_pair);

        MarkovPair end_pair = get_markov_pair(database, "Word \"" ~ words.retro.front ~ "\"", "End");
        insert_markov_pair(database, end_pair);

        log_debug(LoggingScope.Markov, "Added a sentence to the markov model");
    }

    void request_exit()
    {
        should_exit = true;
        send(markov_tid, false);
    }

    void retrain_model()
    {
        database.start_transaction();
        scope(success) database.commit_transaction();
        scope(failure) database.rollback_transaction();

        // There should only ever be one markov retrain be in progress at a time.
        synchronized
        {
            log_info(LoggingScope.Markov, "Retraining markov model...");
            this.database.hotzenbot_exec("DELETE FROM Markov;");
            markov_model.clear();

            auto twitch_messages = this.database.hotzenbot_exec("SELECT message FROM TwitchLog;").map!(x => x["message"].as!string);
            auto irc_messages = this.database.hotzenbot_exec("SELECT message FROM IrcLog;").map!(x => x["message"].as!string);

            foreach (sentence; chain(twitch_messages, irc_messages))
            {
                try
                {
                    add_sentence_to_markov(this.database, sentence);
                }
                catch (Exception e)
                {
                    log_warn(LoggingScope.Markov, "Unable to add sentence to markov model: " ~ e.to!string);
                }
            }

            log_info(LoggingScope.Markov, "Finished retraining markov model.");
        }
    }

    void markov_thread_worker()
    {
        scope(exit) {
            log_warn(LoggingScope.Markov, "Markov worker loop exited");
        }

        markov_tid = thisTid;

        while (!should_exit)
        {
            try
            {
                log_debug(LoggingScope.Markov, "Waiting for sentence");
                receive(
                    (RetrainMessage _)
                    {
                        retrain_model();
                    },
                    (string sentence)
                    {
                        this.add_sentence_to_markov(this.database, sentence);
                    },
                    (bool b)
                    {
                        // Exit markov thread
                    }
                    );
            }
            catch (Exception e)
            {
                log_error(LoggingScope.Markov, "Unable to add sentence to markov: " ~ e.to!string);
            }
        }
    }

    Database* database;
    shared bool should_exit = false;
}

void markov_add_sentence(string sentence)
{
    send(markov_tid, sentence);
}

void request_retrain_markov()
{
    send(markov_tid, MarkovThread.RetrainMessage());
}
