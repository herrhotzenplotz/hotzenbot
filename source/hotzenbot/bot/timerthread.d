/* Copyright 2020 Nico Sonack
 *
 * The contents of this file are subject to the terms of the Common
 * Development and Distribution License, Version 1.0 only (the
 * "License").  You may not use this file except in compliance with
 * the License.
 *
 * See the file LICENSE in this distribution for details.  A copy of
 * the CDDL is also available via the Internet at
 * http://www.opensource.org/licenses/cddl1.txt
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file LICENSE from this distribution.
 */

module hotzenbot.bot.timerthread;

import hotzenbot.commands.expressionparser;
import hotzenbot.commands.timercontext;
import hotzenbot.sys.database;
import hotzenbot.sys.logging;
import hotzenbot.sys.transport;

import std.algorithm;
import std.array;
import std.concurrency;
import std.conv : to;
import std.datetime;

import core.thread;

import d2sqlite3;

struct OneshotTimer
{
    string network;
    string channel;
    string user_name;
    string message;
    SysTime due_time;
    long id;
}

struct PeriodicTimer
{
    string network;
    string channel;
    string expression;
    Duration duration;
    SysTime next_trigger_time;
    long id;
}

__gshared Tid timer_tid;

class TimerThread : Thread
{
    OneshotTimer[] oneshot_timers;
    PeriodicTimer[] periodic_timers;
    Transport[string] transports;
    Database* database;

    this(Database* database)
    {
        super(&timer_worker);
        this.database = database;

        auto single_query_result = database.hotzenbot_exec("SELECT * FROM Timers;");
        foreach (row; single_query_result)
        {
            try
            {
                oneshot_timers ~= OneshotTimer(
                    row["network"].as!string,
                    row["channel"].as!string,
                    row["userName"].as!string,
                    row["message"].as!string,
                    SysTime.fromISOString(row["dueTime"].as!string),
                    row["id"].as!long);
            }
            catch (Exception e)
            {
                log_error(LoggingScope.Timers, "Unable to initialize a timer: " ~ e.to!string);
            }
        }

        auto periodic_query_result = database.hotzenbot_exec("SELECT * FROM PeriodicTimers;");
        foreach (row; periodic_query_result)
        {
            try
            {
                periodic_timers ~= PeriodicTimer(
                    row["network"].as!string,
                    row["channel"].as!string,
                    row["expression"].as!string,
                    row["duration"].as!long.seconds,
                    SysTime.fromISOString(row["nextTriggerTime"].as!string),
                    row["id"].as!long);
            }
            catch (Exception e)
            {
                log_error(LoggingScope.Timers, "Unable to initialize a periodic timer: " ~ e.to!string);
            }
        }
    }

    void add_transport_layer(string name, Transport transport)
    {
        if (name !in transports)
        {
            transports[name] = transport;
        }
        else
        {
            log_info(LoggingScope.Timers, "Transport " ~ name ~ " already registered");
        }
    }

    void timer_worker()
    {
        timer_tid = thisTid;

        while (!should_exit)
        {
            log_debug(LoggingScope.Timers, "Waiting for a timer to be set");
            receiveTimeout(dur!"seconds"(10),
                           (OneshotTimer t) {
                               database.hotzenbot_exec(
                                   "INSERT INTO Timers (network, channel, userName, message, dueTime) VALUES (:net, :chan, :usr, :msg, :due);",
                                   t.network,
                                   t.channel,
                                   t.user_name,
                                   t.message,
                                   t.due_time.toISOString);
                               t.id = database.lastInsertRowid;
                               oneshot_timers ~= t;
                           },
                           (PeriodicTimer t) {
                               database.hotzenbot_exec(
                                   "INSERT INTO PeriodicTimers (network, channel, expression, duration, nextTriggerTime) VALUES (:net, :chan, :expr, :dur, :next);",
                                   t.network,
                                   t.channel,
                                   t.expression,
                                   t.duration.total!"seconds",
                                   t.next_trigger_time.toISOString);

                               t.id = database.lastInsertRowid;
                               periodic_timers ~= t;
                           });

            log_debug(LoggingScope.Timers, "Handling timers");
            auto now = Clock.currTime().toUTC;
            foreach (timer; oneshot_timers.filter!(x => x.due_time <= now))
            {
                log_info(LoggingScope.Timers, "Timer for user " ~ timer.user_name ~ " triggered.");
                transports[timer.network].say(
                    timer.channel,
                    timer.message);

                database.hotzenbot_exec("DELETE FROM Timers WHERE id = :id;", timer.id);
            }

            oneshot_timers = oneshot_timers.filter!(x => x.due_time > now).array;


            for (int i = 0; i < periodic_timers.length; ++i)
            {
                auto timer = periodic_timers[i];
                if (timer.next_trigger_time > now)
                {
                    continue;
                }

                log_info(LoggingScope.Timers, "Handling periodic timer for channel " ~ timer.channel);

                TimerContext ctx = new TimerContext(database);
                auto parser = ExpressionParser(timer.expression);
                auto parse_result = parser.parse();
                if (parse_result.is_error)
                {
                    transports[timer.network].say(
                        timer.channel,
                        parse_result.err_msg);
                }

                auto eval_result = parse_result.result.evaluate(ctx);
                transports[timer.network].say(
                    timer.channel,
                    eval_result.is_error
                    ? eval_result.err_msg
                    : eval_result.result);

                periodic_timers[i].next_trigger_time += periodic_timers[i].duration;
                try
                {
                    database.hotzenbot_exec("UPDATE PeriodicTimers SET nextTriggerTime = :time WHERE id = :id;",
                                            periodic_timers[i].next_trigger_time.toISOString,
                                            timer.id);
                    log_debug(LoggingScope.Timers, "Rescheduled periodic timer for channel " ~ timer.network ~ "/#" ~ timer.channel);
                }
                catch (Exception e)
                {
                    log_error(LoggingScope.Timers, "Unable to schedule periodic timer: " ~ e.to!string);
                }
            }
        }

        log_warn(LoggingScope.Timers, "Timer loop exited");
    }

    void request_exit()
    {
        this.should_exit = true;
    }

    shared bool should_exit = false;
}

void add_oneshottimer(OneshotTimer t)
{
    send(timer_tid, t);
}

void add_periodictimer(PeriodicTimer t)
{
    send(timer_tid, t);
}
