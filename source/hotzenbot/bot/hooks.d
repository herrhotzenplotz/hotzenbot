/* Copyright 2020 Nico Sonack
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License, Version 1.0 only
 * (the "License").  You may not use this file except in compliance
 * with the License.
 *
 * See the file LICENSE in this distribution for details.
 * A copy of the CDDL is also available via the Internet at
 * http://www.opensource.org/licenses/cddl1.txt
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the License file LICENSE from this distribution.
 */

module hotzenbot.bot.hooks;

import std.conv;

import d2sqlite3;

import hotzenbot.sys.database;
import hotzenbot.sys.logging;

struct Hook
{
    string network;
    string channel;
    string trigger_word;
    string code;
    int times;
    long id;
}

void load_hooks_from_database(Database* db, ref Hook[] hooks)
{
    try
    {
        auto query_result = db.hotzenbot_exec("SELECT * FROM Hooks;");

        foreach (row; query_result)
        {
            hooks ~= Hook(row["network"].as!string,
                          row["channel"].as!string,
                          row["triggerWord"].as!string,
                          row["code"].as!string,
                          row["times"].as!int,
                          row["id"].as!long);
        }
    }
    catch (Exception e)
    {
        log_error(LoggingScope.System,
                  "Unable to load hooks from the database: " ~ e.to!string);
    }
}
